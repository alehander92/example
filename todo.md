
* type : ls ping
* load into ruby objects
* `ls{/a/}[0].size`

-> some ruby code and error reporting eventually


ls: list files
  default: -la
  type:
    "total" <count:UInt>
    <perm: Perm> _ <owner_name:Name> <group_name:Name> <size:Size> <date:Date> <path:Path>
    ..
  search-by: path

Ls { count: UInt, @items: LsItem { .. } }

invoke in ruby and evaluate


    "total" <count:UInt>
    <perm: Perm> _ <owner_name:Name> <group_name:Name> <size:Size> <date:Date> <path:Path>
    ..


* fix colors
* error handling
* run programs in $PATH
* history support
* use as terminal
* fix ping/other examples
* types

~ error handling
~ fix ping/other?
~ run programs in $PATH ? maybe another like ls! or ls_ or generate path-based functions
~ history (readline)
~ colors
~ types

* PATH

