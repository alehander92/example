require 'readline'
require 'colorize'
require 'coolline'
require 'coderay'


# ruby prototype:
#   those ideas are applicable
#   for a custom language shell
#
# 1: add some simple commands:
  # ls
  # pwd
  # cd
  # custom binaries (fallback):
  #   just return raw text/run
  # eventually later:
  #   vc (git)
  #   piping: what to do here
# 2: run them using the old repl-like approach
# 3: try to make completions work

# for now directly define here TODO refactor
# forgive me but helix is easier

# ok ls very basic version done
# but we can reuse a lot of that
# for other commands
# we mostly need type/name of columns defs
# and simple impls for each of those
# all the other should be lifted automatically
# for now
# 2 is kinda ok
# 3 : what about completions
# leave them for other time

$env = {}

class Command
end

# TODO more advanced command/type/data specific
# column size logic
def column_cell(column, size)
  ' ' + column.ljust(size - 1)
end

def table_row(columns, column_sizes)
  '|' + columns.zip(column_sizes).map do |(c, size)|
	  column_cell(c.to_s, size)
	end.join('|') + '|'
end

class Res
  attr_reader :column_types, :column_names, :results

  def initialize(columns, results)
    @column_types = columns.map { |c| c[0] }
		@column_names = columns.map { |c| c[1] }
    @results = results
	  @column_sizes = @column_types.map { |t| column_size(t) }
  end

  def [](index)
	  @results[index]
	end

  def column_size(typ)
	  case typ
		when :name
	    10
	  when :size
	    8
	  when :filename
	    32
	  else
	    18
	  end
	end

  def header
    table_row(@column_names, @column_sizes) + "\n" + '-' * 70
  end

  def to_s
    results_text = @results.map { |r| to_text(r, @column_sizes) }.join("\n")
    "#{header}\n#{results_text}\n"
  end

	def to_text(res, column_sizes)
	  table_row(res.column_values, column_sizes)
	end
end
    
# maybe no need, but keep for now
# if we decide to have all results
# share some api
class FileResult
	def initialize(name, file_stat)
	  @name = name
    @file_stat = file_stat
  end

  def self.columns
	  [
		  [:name, 'owner'], [:name, 'group'],
      [:size, 'size'], [:filename, 'filename']
    ]
	end

  def owner
	  owner_name
	end

	def group
		group_name
	end

	def size
		@file_stat.size
	end

	def filename
		@name
	end
		
	def to_s
	  # TODO think of renderers
    Res.new(self.class.columns, [self]).to_s
	end

  def to_name(id)
	  `id -nu #{id}`.strip
	end

  def owner_name
	  to_name(@file_stat.uid)
	end

	def group_name
	  to_name(@file_stat.gid)
	end

	def column_values
		self.class.columns.map do |(_, name)|
		  self.send name.to_sym
		end
  end 
end

class LsCommand < Command
  attr_reader :path

  def initialize(path)
    @path = path
  end

  def to_file(filename)
	  FileResult.new(filename, File.stat(filename))
	end

  def run(env)
    # very advanced much api
    filenames = Dir.entries(@path)
    files = filenames.map { |n| to_file(n) }
	  # no idea how to check the lsp haha
  
    # including some metadata about original
    # request in result?
    # or history of commands sufficient
    # probably not with piping/chains
    Res.new(FileResult.columns, files)
  end
end

# not the same as the bash command, just
# reusing the name for now
# TODO: more detailed args
# and some metaprogramming to 
# autogen those from classes maybe

def ls(path)
  LsCommand.new(path).run($env)
end

def start
  # Readline.pre_input_hook = ->(input) do 
    # Readline.insert_text 'program'
    # Readline.redisplay
  # end
  reader = Coolline.new do |c|
    c.transform_proc = proc do
      CodeRay.scan(c.line, :ruby).term
    end
 end

  loop do
    # $stdout.write "#{$path}> "	
    line = reader.readline("#{$path}> ".blue) # Readline.readline("#{$path}> ".blue, true)
    if !line
      break
    end
    # line = $stdin.readline
    begin
      eval("puts #{line}")
    rescue NameError => e
      puts "error can't find program or run line".red
			raise e
    rescue => e
      puts "error #{e}".red
    rescue SyntaxError => e
      puts "error #{error}".red
    end
  end
end

start
