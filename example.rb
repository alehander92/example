
# for now: notes
# 
# parsing

# parse into Definitions
def parse(source)
  lines = source.split("\n")
  definitions = []
  definition = nil
  type_text = ""
  in_type = false
  i = 0
  while i < lines.length
    line = lines[i]
    # p "line #{line} in_type #{in_type}"
    if !in_type && line.start_with?('program')
      # program ls: list files
      tokens = line.split(' ')
      if tokens.length < 3 #|| tokens[1][-1] != ':'
        raise ExampleError.new("line #{line}: expected program <name>: <doc> syntax")
      end
      args = []
      if tokens[1][-1] != ':'
        if tokens.length < 4 || tokens[2][-1] != ':' || tokens[2].length < 4 || tokens[2][0] != '<' || !(tokens[2][-2] == '>' || tokens[2][-3] == '>' && tokens[2][-2] == '?')
          raise ExampleError.new("line #{line}: expected program <name> <<args>>[?]..: <doc> syntax")
        end
#        args.push(tokens[1][1])
      end
      name_token = tokens[1]
      name = name_token[0 .. -1]
      arg_token = tokens[2]
      arg = arg_token[1 .. -2]
      if arg[-1] == '>'
        args = [arg[0 .. -2]]
      elsif arg[-2 .. -1] == '>?'
        args = [arg[0 .. -3]]
      else
        args = []
      end
      doc = tokens[3 .. -1].join(' ')
      if !definition.nil?
        definitions.push(definition)
      end
      definition = Definition.new(name, args, doc)
    elsif !in_type
      tokens = line.split(' ')
      if tokens.length > 0
        field_token = tokens[0]
        if field_token[-1] != ':'
          raise ExampleError.new("line #{line}: expected <field>: <value> syntax")
        end
        field = field_token[0 .. -2]
        # p field
        case field
        when "default"
          definition.default = tokens[1]
        when "default-flags"
          definition.default_flags = tokens[1 .. -1].join(' ')
        when "type"
          if tokens.length > 1
            definition.type = parse_type(tokens[1])
          else
            definition.type = nil
            type_text = ""
            in_type = true
          end
        when "search-by"
          definition.search_by = tokens[1]
        else
          raise ExampleError.new("line #{line}: field not correct: #{field}")
        end
      end
    else
      if line.length < 4 || line[0 .. 3] != '    '
        in_type = false
        definition.type = parse_type(type_text)
        next
      end
      text = line[4 .. -1]
      type_text += text + "\n"
    end
    i += 1
  end
  if !definition.nil?
    definitions.push(definition)
  end
  definitions
end

class TypeBoxMappings
  attr_reader :boxes, :map, :has_lines
  	
  # boxes: [TypeBox?]
  # map: {String: TypeBox}

  # TODO: for different args, different types/one type, different boxes?
  def initialize(boxes=[], map={}, has_lines=false)
    @map = map
    @has_lines = has_lines
    @boxes = boxes
  end
end

class TypedBox
  attr_reader :kind, :name, :type, :text

  def initialize(kind, name_or_text, type)
    @kind = kind
    @name = name_or_text
    @type = type
    @text = name_or_text
  end
end

# parse token
#
#   <name:Type> -> [TypedBox] or Typed
def parse_token(token)
  boxes = []
  state = :none
  i = 0
  while i < token.length + 1
    c = token[i]
    case state
    when :none
      if c.nil?
        break
      elsif c != '<'
        state = :text
        text = c
      else
        state = :typed_name
        name = ""
        type = ""
      end
    when :text
      if !c.nil? && c != '<'
        text += c
      else
        boxes.push(TypedBox.new(:text, text, nil))
        state = :none
        next
      end
    when :typed_name
      if !c.nil? && c != ':'
        name += c
      else
        state = :typed_type
        type = ''
      end
    when :typed_type
      if !c.nil? && c != '>'
        type += c
      else
        if name.empty? || type.empty?
          raise ExampleError.new("token #{token} expected <<name>:<Type>> syntax, empty name or type found")
        end
        boxes.push(TypedBox.new(:typed, name, type))
        name = ''
        type = ''
        state = :none
      end
    end
    i += 1
  end
  # if token.length < 5 || token[0] != '<' || token[-1] != '>'
    # raise ExampleError.new("token #{token} expected <<name>:<Type>> syntax")
  # end
  # tokens = token.split(':')
  # name = tokens[0][1 .. -1]
  # type = tokens[1][0 .. -2]
  # TypedBox.new name, type
  boxes
end

# parse 
# 
#   type_line*
#   
#   type_line is <name:Type> text=<name:Type> .. [*]
#
#   returns TypeBoxMappings with the expected maps and types and flag for has_lines
def parse_type(source)
  #p "parse_type ", source
  boxes = []
  map = {}
  tokens = source.split(' ')
  has_lines = false
  if tokens.length > 0 && tokens[-1] == '*'
    last_index = tokens.length - 2
  else
    last_index = tokens.length - 1
  end
  tokens.each_with_index do |token, i|
    if i <= last_index
      if token != '_'
        type_boxes = parse_token(token)
        boxes += type_boxes
        # p token
        # p boxes
        type_boxes.each do |box|
          if box.kind == :typed
            map[box.name] = box
          end
        end
      else
        boxes.push(nil)
      end
      if i < last_index
        boxes.push(TypedBox.new(:whitespace, "", nil))
      end
    else
      has_lines = true
    end
  end
  TypeBoxMappings.new boxes, map, has_lines
end

class Definition
  attr_reader :name, :args, :doc
  attr_accessor :default, :default_flags, :type, :search_by
	
  # TODO: for different args, different types/one type, different box?
  def initialize(name, args, doc)
    @name, @args, @doc = name, args, doc
    @default = ""
    @default_flags = ""
    @type = nil
    @search_by = ""
  end
end

class ExampleError < Exception
end

def emit(definitions)
  definitions.each_with_index.map do |definition, i|
    fields = definition.type.boxes.filter do |box|
      box.nil? || box.kind == :typed
    end.map do |box|
      if box.nil?
        nil
      else
        box.name
      end
    end
    fields_text = fields.compact.map { |field| ":#{field}" }.join(", ")
    box_names = fields.filter do |field|
      !field.nil?
    end.join(", ")

    if !fields_text.empty?
      attr_text = "  attr_reader #{fields_text}\n\n"
    else
      attr_text = ""
    end
   
    args_text = definition.args.join(', ')
    if definition.type.has_lines
      result = "class #{definition.name.capitalize} < LinesBase\n"
      result += "  def initialize(lines)\n"
      result += "    super lines, [#{fields_text}]\n"
      result += "  end\n"
      result += "\n"
      result += "end\n\n"

      result += "class #{definition.name.capitalize}Line\n"
      result += attr_text
      result += "  def initialize(#{box_names})\n"
      fields.compact.each do |field|
        result += "    @#{field} = #{field}\n"
      end
      result += "  end\n\n"
      result += "  def to_s\n"
      code = fields.compact.map do |field|
        "    @#{field}.to_s + \"\\t\" "
      end.join(" + ")
      result += "    #{code}\n"
      result += "  end\n"
      result += "end\n\n"
      
      result += "def #{definition.name}(#{args_text})\n"
      result += "  call_program($definitions[#{i}], #{args_text})\n"
      result += "end\n"
    end
      
  end.join("\n")
end

def load(value, box)
  case box.type
  when 'Int', 'Size'
    value.to_i
  when 'Text', 'Name', 'Path', 'Ip'
    value
  when 'Float'
    value.to_f
  else
    value
  end
end

def process(text, definition)
  lines = text.split("\n")
  line_results = lines.each_with_index.map do |line, i|
    line_class = eval("#{definition.name.capitalize}Line")
    # for each box
    # try to parse the next characters as this kind
    # if not possible, return nil TODO error
    i1 = 0 # index in line
    i2 = 0 # index in box or count of loops for box

    can_parse = true
    args = []
    definition.type.boxes.each_with_index do |box, box_i|
      i2 = 0
      value = ''
      can_parse = true
      # p "box #{box.inspect} #{line[i1 .. -1]}"
      while i1 < line.length + 1
        c = line[i1]
        # p "c #{i1} #{c}"
        if box.nil?
          kind_or_nil = nil
        else
          kind_or_nil = box.kind
        end
        case kind_or_nil
        when :text
          # p "#{i1} #{i2} #{c} #{box}"
          if i2 >= box.text.length
            break
          end
          if c != box.text[i2]
            can_parse = false
            break
          end
          i2 += 1
        when :whitespace
          if !!(c =~ /[ \t\n]/)
            i2 += 1
          else
            if i2 == 0
              can_parse = false
            end
            break
          end
        when :typed, nil
          # p "typed #{i1} #{i2} #{c} #{box} #{!!(c =~ /[ \t\n]/)}"
          if c.nil? || !!(c =~ /[ \t\n]/) || 
              box_i < definition.type.boxes.length - 1 && definition.type.boxes[box_i + 1].kind == :text &&
              definition.type.boxes[box_i + 1].text[0] == c
            if i2 == 0
              can_parse = false
              break
            end
            if !kind_or_nil.nil?
              args.push(load(value, box))
              # p args, args.length, " ", i2
            end
            break
          else
            if !kind_or_nil.nil?
              value += c
            end
            i2 += 1
          end
        end
        i1 += 1
      end
      if !can_parse
        break
      end
    end

    if can_parse
      begin
        # p "new ", args
        line_class.new(*args) # *(line.split(/[ \t\n]/).filter { |word| !word.empty? }))
      rescue ArgumentError
        nil
      end
    else
      nil
    end
  end.compact
  #p line_results[0]
  process_class = eval("#{definition.name.capitalize}")
  process_class.new line_results
end

def call_program(definition, *args)
  args_text = args.map(&:to_s).join(' ')
  program_with_args = "#{definition.name} #{definition.default_flags} #{args_text}"
  # p program_with_args
  result = `#{program_with_args}`
  #puts result
  processed_result = process(result, definition)
  processed_result
end

class LinesBase
  attr_reader :lines, :fields

  def initialize(lines, fields)
    @lines = lines
    @fields = fields
  end
  
  def [](index)
    @lines[index]
  end
  
  def to_s
    result = ""
    result += (@fields.map { |field| field.to_s }.join("\t") + "\n").blue
    result += @lines.map do |line|
      line.to_s + "\n"
    end.join
    result
  end
end


# a shell program : `cd` doesn't seem to change our ruby path
def cd(directory="~")
  path = File.expand_path directory
  # p path
  Dir.chdir path
  $path = Dir.pwd
  nil
end

$definitions = []
$path = Dir.pwd

def method_missing(name, *args)
  # p name
  if !name.start_with?('to') && !name.start_with?('load')
    program = name.to_s
    found = false
    result = ''
    # p "method_missing #{program}"
    args_text = args.join(' ')
    ENV['PATH'].split(':').each do |path|
      program_full_path = File.join(path, program)
      if File.exists?(program_full_path)
        # TODO stdout hide/return result?
        # currently system for interactive programs like `bible`
        # backticks for normal like `cat`
        # result = system "#{program_full_path} #{args_text}"
        result = `#{program_full_path} #{args_text}`
        # puts result
        found = true
        break
      end
    end
    if !found
      raise ExampleError.new("can't run #{program} as program")
    end
    result
  end
end

require 'readline'
require 'colorize'
require 'coolline'
require 'coderay'

def start
  definitions = parse(File.read("definitions.example"))
  $definitions = definitions
  base_source = emit(definitions)
  # puts base_source
  eval(base_source)
  # Readline.pre_input_hook = ->(input) do 
    # Readline.insert_text 'program'
    # Readline.redisplay
  # end
  reader = Coolline.new do |c|
    c.transform_proc = proc do
      CodeRay.scan(c.line, :ruby).term
    end
  end

  loop do
    # $stdout.write "#{$path}> "
    line = reader.readline("#{$path}> ".blue) # Readline.readline("#{$path}> ".blue, true)
    if !line
      break
    end
    # line = $stdin.readline
    begin
      eval("puts #{line}")
    rescue NameError => e
      # try to run the command from $PATH
    
      # line_without_newline = line.strip
      # after_program_space_index = line_without_newline.index(/[ \t\n]/)
      # if !after_program_space_index.nil?
      #   program = line_without_newline[0 .. after_program_space_index - 1]
      #   args = line_without_newline[after_program_space_index + 1 .. -1]
      # else
      #   program = line_without_newline
      #   args = ''
      # end
      # found = false
      # # p program
      # p args
      if !found
        puts "error can't find #{program} or run line".red
      end
    rescue => e
      puts "error #{e}".red
    rescue SyntaxError => e
      puts "error #{error}".red
    end
  end
  # puts ls[0].path
end

start



 
  
